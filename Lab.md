## Lab excercise

1. Setup Eclipse
2. Install MySQL
3. Create a database called ``emp_db``
4. Clone the hibernate-examples repo 
   ```
   git clone git@gitlab.com:classpath-hibernate/hibernate-examples.git
   ```
5. work on all the examples covered
   1. hello-world
   2. one-to-one
   3. one-to-many
   4. many-to-many
   5. Inheritance mapping
      1. table per hirearchy
      2. table per concrete class
5. Eclipse project structure 

```
project
│   
└───src
│   │   main
|   |   |
|   |   |__ java
|   |   |    |
|   |   |    |__ com
|   |   |       |
|   |   |       |__ classpath
|   |   |          |
|   |   |          |__ hibernate       
|   |   |             |
|   |   |             |__ client       
|   |   |             |
|   |   |             |__ model       
|   |   | 
|   |   |  
|   |   |__ resources
|   |         hibernate-cfg.xml
|___
    │  pom.xml
    
```

 6. Maven dependencies
 ```xml
 <dependencies>
        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-core</artifactId>
            <version>5.4.4.Final</version>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.17</version>
        </dependency>
    </dependencies>
 ```       
